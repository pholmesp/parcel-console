import React from "react";
import { inject, observer } from "mobx-react";
import { Switch, Route, withRouter, Link } from "react-router-dom";
import { Row, Col, Form, Icon, Input, Button, Checkbox } from 'antd';

// const formItemLayout = {
//   labelCol: {
//     xs: { span: 24 },
//     sm: { span: 3 }
//   },
//   wrapperCol: {
//     xs: { span: 24 },
//     sm: { span: 21 }
//   }
// };

@Form.create({ name: 'login' })
@inject('authStore')
@withRouter
@observer
export default class Login extends React.Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        this.props.authStore.login(values.username, values.password)
        // .then(() => this.props.history.push("/"));
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <React.Fragment>
        <Row style={{background:'#FFF', marginTop:100, padding: 10}}>
          <Form onSubmit={this.handleSubmit}>
            <Row type='flex' justify='space-around' align='middle'>
              <Col xs = {{span : 16}} lg = {{span : 8}}>
              <h1>Ming Tong</h1>
              </Col>
            </Row>
            <Row type='flex' justify='center' align='middle'>
              <Col xs = {{span : 16}} lg = {{span : 8}}>
                <Form.Item>
                  {getFieldDecorator('username', {
                    rules: [{ required: true, message: 'Please input your username!' }],
                  })(
                    <Input
                      size='large'
                      
                      prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                      placeholder="Username"
                    />,
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row type='flex' justify='space-around' align='middle'>
              <Col xs = {{span : 16}} lg = {{span : 8}}>
                <Form.Item>
                  {getFieldDecorator('password', {
                    rules: [{ required: true, message: 'Please input your Password!' }],
                  })(
                    <Input
                      size='large'
                      prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                      type="password"
                      placeholder="Password"
                    />,
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row type='flex' justify='space-around' align='middle'>
              <Col xs = {{span : 16}} lg = {{span : 8}}>
                <Form.Item>
                  <Button type="primary" htmlType="submit" style={{ width: '100%', height: '35px' }}>
                    Log in
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Row>
      </React.Fragment>
    );
  }
}