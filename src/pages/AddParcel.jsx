import React from 'react';
import { inject, observer } from 'mobx-react';
import { Switch, Route, withRouter, Link } from 'react-router-dom';
import { Input, Descriptions, List, Row, AutoComplete, Button, Form, DatePicker, Spin} from 'antd';

import moment from 'moment'

const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 3 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 21 },
    },
  };
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 12,
      },
    },
  };
function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
  }
  
@Form.create({ name: 'parcel' })
@inject('parcelsStore', 'queryStore')
@withRouter
@observer
export default class AddParcel extends React.Component {
    state = {
        validation: false
    }
    componentDidMount() {
        // To disabled submit button at the beginning.
        this.props.form.validateFields();
      }
    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
          if (!err) {
            console.log('Received values of form: ', values);
            this.props.parcelsStore.addParcel()
          }
        });
      };

    render(){
        const { parcelsStore, queryStore} = this.props
        const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched} = this.props.form;
        return(
            <React.Fragment>
                <Row style={{background:'#FFF', marginTop:5, padding: 10}}>
                <Spin spinning={queryStore.isLoading}>
                    <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                        <Form.Item label="订单号" 
                            validateStatus={isFieldTouched('orderId') && getFieldError('orderId') ? 'error': ''}
                            help={isFieldTouched('orderId') && getFieldError('orderId') || ''} 
                            >
                            {getFieldDecorator('orderId', {
                                rules: [
                                {
                                    required: true,
                                    message: '请输入订单号',
                                },
                                ],
                            })(
                                <Input size="large" id="orderId" value={parcelsStore.parcel.orderId} onChange={(e) => parcelsStore.updateParcel(e.currentTarget.id, e.currentTarget.value)} />
                            )}
                        </Form.Item>

                        <Form.Item label="打包编号"
                            validateStatus={isFieldTouched('parcelId') && getFieldError('parcelId') ? 'error': ''}
                            help={isFieldTouched('parcelId') && getFieldError('parcelId') || ''}
                            >
                            {getFieldDecorator('parcelId', {
                                rules: [
                                {
                                    required: true,
                                    message: '请输入打包编号',
                                },
                                ],
                            })(
                                <Input size="large" id="parcelId" onChange={(e) => parcelsStore.updateParcel(e.currentTarget.id, e.currentTarget.value)} /> 
                            )}
                        </Form.Item>

                        <Form.Item label="快递名称"
                            validateStatus={isFieldTouched('expressName') && getFieldError('expressName') ? 'error': ''}
                            help={isFieldTouched('expressName') && getFieldError('expressName') || ''}
                            >
                            {getFieldDecorator('expressName', {
                                rules: [
                                {
                                    required: true,
                                    message: '请输入快递名称',
                                },
                                ],
                            })(
                                <Input size="large" id="expressName" onChange={(e) => parcelsStore.updateParcel(e.currentTarget.id, e.currentTarget.value)} />
                            )}
                        </Form.Item>

                        <Form.Item label="快递单号" 
                            validateStatus={isFieldTouched('expressId') && getFieldError('expressId') ? 'error': ''}
                            help={isFieldTouched('expressId') && getFieldError('expressId') || ''}
                            >
                            {getFieldDecorator('expressId', {
                                rules: [
                                {
                                    required: true,
                                    message: '请输入快递单号',
                                },
                                ],
                            })(
                                <Input size="large" id="expressId" onChange={(e) => parcelsStore.updateParcel(e.currentTarget.id, e.currentTarget.value)} />
                            )}
                        </Form.Item>

                        <Form.Item label="长度(CM)" 
                            validateStatus={isFieldTouched('length') && getFieldError('length') ? 'error': ''}
                            help={isFieldTouched('length') && getFieldError('length') || ''}
                            >
                            {getFieldDecorator('length', {
                                rules: [
                                    {
                                        type: 'number',
                                        message: '请输入有效的数字，比如 12.34. ',
                                        transform(value) {
                                            return Number(value);
                                        }
                                    },
                                    {
                                        required: true,
                                        message: '请输入长度(CM)',
                                    }
                                ],
                            })(
                                <Input size="large" id="length" value={parcelsStore.parcel.length} onChange={(e) => parcelsStore.updateParcel(e.currentTarget.id, e.currentTarget.value)} />
                            )}
                        </Form.Item>

                        <Form.Item label="宽度(CM)"
                            validateStatus={isFieldTouched('width') && getFieldError('width') ? 'error': ''}
                            help={isFieldTouched('width') && getFieldError('width') || ''}
                            >
                            {getFieldDecorator('width', {
                                rules: [
                                    {
                                        type: 'number',
                                        message: '请输入有效的数字，比如 12.34. ',
                                        transform(value) {
                                            return Number(value);
                                        }
                                    },
                                {
                                    required: true,
                                    message: '请输入宽度(CM)',
                                },
                                ],
                            })(
                                <Input size="large" id="width" onChange={(e) => parcelsStore.updateParcel(e.currentTarget.id, e.currentTarget.value)} />
                            )}
                        </Form.Item>

                        <Form.Item label="高度(CM)"
                            validateStatus={isFieldTouched('height') && getFieldError('height') ? 'error': ''}
                            help={isFieldTouched('height') && getFieldError('height') || ''}
                            >
                            {getFieldDecorator('height', {
                                rules: [
                                    {
                                        type: 'number',
                                        message: '请输入有效的数字，比如 12.34. ',
                                        transform(value) {
                                            return Number(value);
                                        }
                                    },
                                {
                                    required: true,
                                    message: '请输入高度(CM)',
                                },
                                ],
                            })(
                                <Input size="large" id="height" onChange={(e) => parcelsStore.updateParcel(e.currentTarget.id, e.currentTarget.value)} />
                            )}
                        </Form.Item>

                        <Form.Item label="体积(立方米)"
                            validateStatus={isFieldTouched('volume') && getFieldError('volume') ? 'error': ''}
                            help={isFieldTouched('volume') && getFieldError('volume') || ''}
                            >
                            {getFieldDecorator('volume', {
                                rules: [
                                    {
                                        type: 'number',
                                        message: '请输入有效的数字，比如 12.34. ',
                                        transform(value) {
                                            return Number(value);
                                        }
                                    },
                                {
                                    required: true,
                                    message: '请输入体积(立方米)',
                                },
                                ],
                            })(
                                <Input size="large" id="volume" onChange={(e) => parcelsStore.updateParcel(e.currentTarget.id, e.currentTarget.value)} />
                            )}
                        </Form.Item>

                        <Form.Item label="重量(KG)"
                            validateStatus={isFieldTouched('weight') && getFieldError('weight') ? 'error': ''}
                            help={isFieldTouched('weight') && getFieldError('weight') || ''}
                            >
                            {getFieldDecorator('weight', {
                                rules: [
                                    {
                                        type: 'number',
                                        message: '请输入有效的数字，比如 12.34. ',
                                        transform(value) {
                                            return Number(value);
                                        }
                                    },
                                {
                                    required: true,
                                    message: '请输入重量(KG)',
                                },
                                ],
                            })(
                                <Input size="large" id="weight" onChange={(e) => parcelsStore.updateParcel(e.currentTarget.id, e.currentTarget.value)} />
                            )}
                        </Form.Item>

                        <Form.Item label="包装">
                            {getFieldDecorator('packing', {
                                rules: [
                                {
                                    required: false,
                                    message: '请输入包装',
                                },
                                ],
                            })(
                                <Input size="large" id="packing" onChange={(e) => parcelsStore.updateParcel(e.currentTarget.id, e.currentTarget.value)} />
                            )}
                        </Form.Item>

                        <Form.Item label="入库时间"
                            validateStatus={isFieldTouched('storeTime') && getFieldError('storeTime') ? 'error': ''}
                            help={isFieldTouched('storeTime') && getFieldError('storeTime') || ''}
                            >
                            {getFieldDecorator('storeTime', {
                                rules: [
                                {
                                    required: true,
                                    message: '请输入入库时间',
                                },
                                ],
                            })(
                                <DatePicker id="storeTime" size='large' showTime showToday={false} style={{width: '100%'}} onOk={(value) => parcelsStore.updateParcel('parcel_storeTime', moment(value).format('YYYY-MM-DD HH:mm:ss'))} />
                            )}
                        </Form.Item>

                        <Form.Item label="旧包号">
                            {getFieldDecorator('oldParcelId', {
                                rules: [
                                {
                                    required: false,
                                    message: '请输入旧包号',
                                },
                                ],
                            })(
                                <Input size="large" id="oldParcelId" onChange={(e) => parcelsStore.updateParcel(e.currentTarget.id, e.currentTarget.value)} />
                            )}
                        </Form.Item>

                        <Form.Item label="备注">
                            {getFieldDecorator('storeComment', {
                                rules: [
                                {
                                    required: false,
                                    message: '请输入备注',
                                },
                                ],
                            })(
                                <Input size="large" id="storeComment" onChange={(e) => parcelsStore.updateParcel(e.currentTarget.id, e.currentTarget.value)} />
                            )}
                        </Form.Item>

                        <Form.Item {...tailFormItemLayout}>
                            <Button type="primary" htmlType="submit" disabled={hasErrors(getFieldsError())}>添加订单</Button>
                        </Form.Item>
                    </Form>
                    </Spin>
                </Row>
            </React.Fragment>
        )
    }
}