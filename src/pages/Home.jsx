import React from 'react';
import { inject, observer } from 'mobx-react';
import { Switch, Route, withRouter, Link } from 'react-router-dom';
import { Input, Descriptions, List, Row, AutoComplete, Button, Icon, Empty, Spin} from 'antd';
const Search = Input.Search;

const { Option } = AutoComplete;

@inject('historyStore', 'queryStore')
@withRouter
@observer
export default class Home extends React.Component {
    state = {
        options: [...this.props.historyStore.history.keys()], 
    }
    onChange = (value) =>{
        this.props.queryStore.emptyResult = false
        this.props.queryStore.expressId = value
    }
    onSearch = (value) => {
        this.props.queryStore.emptyResult = false
        if(value) this.setState({
            options: [...this.props.historyStore.history.keys()].filter(ele => ele.indexOf(value)>=0)
        })
        else this.setState({
            options: [...this.props.historyStore.history.keys()]
        })
    }
    render(){
        const { options } = this.state
        const { historyStore, queryStore} = this.props
        return(
            <React.Fragment>
                <Row style={{background:'#FFF', marginTop:5, padding: 10}}>
                    <AutoComplete placeholder="Express ID ..." 
                        autoFocus={true}
                        size='large' 
                        value={queryStore.expressId} 
                        style={{ width: '100%' }}
                        onChange={this.onChange}
                        onSelect={value => queryStore.queryRecord(value)}
                        onSearch={this.onSearch}
                        dataSource={
                            options.map(ele=>
                                <Option key={ele}>{ele}</Option>
                                )
                        }
                        enterButton >
                            <Input
                                suffix={
                                <Button
                                    className="search-btn"
                                    style={{ marginRight: -12 }}
                                    size="large"
                                    type="primary"
                                    onClick={() => queryStore.queryRecord(queryStore.expressId)}
                                >
                                    <Icon type="search" />
                                </Button>
                                }
                            />
                    </AutoComplete>
                </Row>
                <Row style={{background:'#FFF', marginTop:5, padding: 10, minHeight:60}}>
                    {console.log(queryStore.queriedRecords)}
                    <Spin spinning={queryStore.isLoading}>
                        {
                            queryStore.emptyResult ? <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                            :
                            queryStore.queriedRecords.map(ele =>
                                <Descriptions bordered title={queryStore.expressId} border size="small" key={ele.idorders} style={{marginTop: 10}}>
                                    <Descriptions.Item label="打包编号">{ele.parcelId}</Descriptions.Item>
                                    <Descriptions.Item label="快递名称">{ele.expressName}</Descriptions.Item>
                                    <Descriptions.Item label="快递单号">{ele.expressId}</Descriptions.Item>
                                    <Descriptions.Item label="长度(CM)">{ele.length}</Descriptions.Item>
                                    <Descriptions.Item label="宽度(CM)">{ele.width}</Descriptions.Item>
                                    <Descriptions.Item label="高度(CM)">{ele.height}</Descriptions.Item>
                                    <Descriptions.Item label="体积(立方米)">{ele.volume}</Descriptions.Item>
                                    <Descriptions.Item label="重量(KG)">{ele.weight}</Descriptions.Item>
                                    <Descriptions.Item label="包装">{ele.packing}</Descriptions.Item>
                                    <Descriptions.Item label="入库时间">{ele.storeTime}</Descriptions.Item>
                                    <Descriptions.Item label="备注" span={3}>{ele.storeComment}</Descriptions.Item>
                                    <Descriptions.Item label="物流信息" span={3}></Descriptions.Item>
                                </Descriptions>
                            )
                        }
                    </Spin>
               
                </Row>
                {/* 75146254346760  7705284758188*/}
                <Row style={{background:'#FFF', marginTop:5, padding: 10}}>
                    {console.log(historyStore.history)}
                    {console.log(historyStore.history.get(queryStore.expressId))}
                   <List
                    size="small"
                    header={<div>Search History:</div>}
                    dataSource={[...historyStore.history.keys()]}
                    renderItem={
                        item => <List.Item actions={[<a onClick={() => queryStore.queryRecord(item)}>Track</a>]} >{item}</List.Item>
                    }
                   >

                   </List>
                </Row>
            </React.Fragment>
        )
    }
}