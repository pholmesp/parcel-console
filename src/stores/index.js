import historyStore from './historyStore'
import queryStore from './queryStore'
import systemStore from './systemStore'
import parcelsStore from './parcelsStore'
import authStore from './authStore'

const stores = {
    historyStore,
    queryStore,
    systemStore,
    parcelsStore,
    authStore
  }

export default stores