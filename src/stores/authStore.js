
import { observable, action, computed, reaction, createTransformer } from 'mobx';
import API_URL from '../config'
import stores from './index'

class AuthStore {
  @observable currentUser = JSON.parse(window.localStorage.getItem('current')||'{}');
  // @observable token = window.localStorage.getItem('jwt');
  @observable isLoading = false;
  @observable errors = undefined;

  // constructor() {
  //   reaction(
  //     () => this.token,
  //     token => {
  //       if (token) {
  //         window.localStorage.setItem('jwt', token);
  //       } else {
  //         window.localStorage.removeItem('jwt');
  //       }
  //     }
  //   );
  // }

  @action login(username, password) {
    this.isLoading = true;
    fetch(`${API_URL.ROOT_API_URL}/login`, {
      method: 'POST',
      headers:{
          'Content-Type': 'application/json'
      },
      body: JSON.stringify({'username': username, 'password': password})
    })
    .then(action(res=>{
      console.log(res)
      if (res.status === 200) {
        return res.json()
      }
      else if(res.status === 401) {
        alert('Email or password is incorrect, please try again ... ')
        console.log(this.currentUser)
      } 
      else {
        stores.systemStore.networkError = true
        stores.systemStore.networkErrorInfo = res
        alert("Sign In Error, please refesh page and try again ... ")
      }
    }))  
    .then(action(user => {
      console.log(user)
      if (user!=undefined) {
        this.currentUser = user
        window.localStorage.setItem('current', JSON.stringify(this.currentUser))
      }
    }))
    .finally(action(() => { this.isLoading = false; }));
  }

  @action logout() {
    window.localStorage.setItem('current', '{}')
    this.currentUser = {};
  }
}

export default new AuthStore();
